// прописываем зависимости
const express        = require("express");
const bodyParser 	 = require("body-parser");
const MongoClient    = require('mongodb').MongoClient;
const app            = express();
const hbs 			 = require("express-handlebars");
//= = = = = = = = = = = = =

//подключение БД
const url = "mongodb://localhost:27017/";
const mongoClient = new MongoClient(url, { useUnifiedTopology: true });
//= = = = = = = = = = = = =

//подключение nbs
app.set("view engine", "hbs");
app.engine("hbs", hbs(
    {
        layoutsDir: "build", 
        defaultLayout: null,
        extname: "hbs"
    }
))
//= = = = = = = = = = = = =
 
// Маршрутизация
app.use(express.static(__dirname + "/public"));

app.use("/main", function(request, response){
     
    response.sendFile(__dirname + "/public/main.html");

});

app.get("/create", function(request, response){
     
    response.sendFile(__dirname + "/public/create/" + "create.html");

});

app.use("/login", function(request, response){
     
    response.sendFile(__dirname + "/public/login/" + "login.html");

});

app.use("/admin", function(request, response){
     
    response.sendFile(__dirname + "/public/admin/" + "admin.html");

});

app.use("/food", function(request, response){
     
    response.sendFile(__dirname + "/public/kate/" + "food.html");

});

app.use("/chemistry", function(request, response){
     
    response.sendFile(__dirname + "/public/kate/" + "chemistry.html");

});

app.use("/cosmetics", function(request, response){
     
    response.sendFile(__dirname + "/public/kate/" + "cosmetics.html");

});

app.use("/public", function(request, response){
     
    response.sendFile(__dirname + "/public/kate/" + "public.html");

});

app.use("/child", function(request, response){
     
    response.sendFile(__dirname + "/public/kate/" + "child.html");

});

app.use("/hobby", function(request, response){
     
    response.sendFile(__dirname + "/public/kate/" + "hobby.html");

});

app.use("/health", function(request, response){
     
    response.sendFile(__dirname + "/public/kate/" + "health.html");

});

app.use("/animals", function(request, response){
     
    response.sendFile(__dirname + "/public/kate/" + "animals.html");

});

app.use("/holidays", function(request, response){
     
    response.sendFile(__dirname + "/public/kate/" + "holidays.html");

});

app.use("/sex", function(request, response){
     
    response.sendFile(__dirname + "/public/kate/" + "sex.html");

});

app.use("/sigi", function(request, response){
     
    response.sendFile(__dirname + "/public/kate/" + "sigi.html");

});
// создаем парсер для данных + добавление в БД

const urlencodedParser = bodyParser.urlencoded({extended: false});

app.post("/create_post", urlencodedParser, function (request, response) {
    if(!request.body) return response.sendStatus(400);

    const Name = request.body.names;
    const Urlpic = request.body.urlpic;
    const Price = request.body.price;
    const Magazine = request.body.magazine;
    const Consist = request.body.consist;
    const Checkbox = request.body.checkbox;

    

    mongoClient.connect(function(err, client){

	    const db = client.db("usersdb");
	    const collection = db.collection("man");

	    collection.find({}).sort({id: -1}).toArray(function(err, players){

        	needId = players[0].id
        	console.log(needId)

	    	did = 1 + Number(needId);
	    	console.log(did)

	    	let man = [{"name": Name, "urlpic": Urlpic, "price": Price, "magazine": Magazine, "consist": Consist, "kate": Checkbox, "id": String(did)}];

	    	collection.insertMany(man, function(err, results){
		    	console.log("\n= = = = = = = = = = = = =\nПроизошла запись в БД:\n")
		        console.log(results);
		        console.log("\n= = = = = = = = = = = = =\n")
		        client.close();
		    });
    	});
	    
	});

});

app.get("/post/:id", function(request, response){
	mongoClient.connect(function(err, client){
	    const db = client.db("usersdb");
	    const collection = db.collection("man");
	    collection.findOne({id: request.params["id"]}, function(err, result){
	               if(err) return console.log(err);
	               console.log(result);  
	               if(result != null)
	                       response.render(__dirname+"/public/post/post.hbs", {
	                           name: result.name,
	                           urlpic: result.urlpic,
	                           price: result.price,
	                           magazine: result.magazine,
	                           consist: result.consist,
	                       })
	                       else
	                       {
	                           response.render(__dirname+"/public/post/post.hbs", {
	                           })
	                       }
	           });
    });
});

app.get("/posts/:id", function(req, res){
    mongoClient.connect(function(err, client){
	    const db = client.db("usersdb");
	    const collection = db.collection("man");
	    console.log(req.params["id"]);

	    collection.findOne({id: req.params["id"]}, function(err, result){

	        if(err) return console.log(err);
	        if(result != null) {
	        	res.send(result);
	        	
	        	console.log("Успешная подгрузка на странице main")
	        }
	    });

    });
});

app.listen(2133);